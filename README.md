# Diamond Dimensions 2
Git repository for the Diamond Dimensions 2 modpack for Minecraft 1.12.2.

# Usage
Run `./zip.sh` and drag it in MultiMC.

# Credits

[Galacticraft wiki (and mod) by micdoodle8 and radfast](https://wiki.micdoodle8.com/wiki/Main_Page)
Original modpack for 1.6.4 by DanTDM.
